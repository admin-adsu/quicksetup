#!/bin/bash
clear
# conf 
    cf_web_serv=http://192.168.200.25
    cf_nano_maincf=/etc/nanorc
    cf_nano_conffiles=/usr/share/nano/conf.nanorc

# init
    wget_command="/usr/bin/wget -qO /tmp/conf.txt ${cf_web_serv}"
    apt_command="apt-get install -y -q=2"

function config_this {
    # INIT
    target_conf_file=$1
    source_conf_file=$2
    key_string=$3
    
    # if conf already contains key string
    if grep -q "$key_string" $target_conf_file ; then
        echo "$target_conf_file already configured" 
        return -1
    fi 

    if [ -e  $target_conf_file ]; then 
        if $wget_command/$source_conf_file; then
            echo ">>>> Set bash user conf"
            cat /tmp/conf.txt >> $target_conf_file && rm -f /tmp/conf.txt
        else 
            echo "error download conf: $cf_web_serv/$source_conf_file"
         fi
    else
        echo "bash user conf skipped, file $target_conf_file not exist"
    fi 
    let target_conf_file=""
    let source_conf_file="" 
    let key_string="" 
}

config_this /home/serg/.bashrc conf/bash/userconf-root.sh "serg conf"  
