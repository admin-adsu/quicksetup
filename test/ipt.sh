#!/bin/bash

webcoders_ru=163.172.142.248
mark_postgres=0x5432

admin_mynorbu_com=89.223.29.41
mark_ssh=0x22

# postgres
echo iptables -A PREROUTING -t mangle -d webcoders.ru -p tcp --dport 5436 -j CONNMARK --set-mark $mark_postgres
echo iptables -A PREROUTING -t nat -m connmark --mark $mark_postgres -j DNAT 5.200.55.65:5432
echo iptables -A PREROUTING -t nat -m connmark --mark $mark_postgres -j SNAT webcoders.ru

# ssh
echo iptables -A PREROUTING -t mangle -d webcoders.ru -p tcp --dport 1998 -j CONNMARK --set-mark $mark_ssh
echo iptables -A PREROUTING -t nat -m connmark --mark $mark_ssh -j DNAT admin.mynorbu.com:22
echo iptables -A PREROUTING -t nat -m connmark --mark $mark_ssh -j SNAT webcoders.ru

