#serg conf
export HISTTIMEFORMAT="%h %d %H:%M:%S " 
export HISTFILESIZE=10000
export HISTSIZE=10000
export HISTCONTROL=ignorespace:erasedups    # не сохранять строки, нач. с пробела и дубликаты
export HISTIGNORE="ls:ps:history:alias*"           # не сохранять такие команды

shopt -s histappend                         # Добавлять новые команды в файл с историей, а не переписывать его каждый раз,
shopt -s cmdhist                            # ОДна команда - одна запись в истории
shopt -s cdspell                            # Исправлять ошибки  в написании
shopt -s cmdhist                            # Разорванные многострочные команды в истории команд

PROMPT_COMMAND='history -a' #  Мгновенно Сохранять Историю Команд"  



# Алиасы (отсюда http://blog.angel2s2.ru/2008/06/bash.html)
# "Заготовки" для 'ls' (предполагается, что установлена GNU-версия ls)

alias ll="/bin/ls -lh --color" # Отображает символьную инфу о типе файла
alias l1="/bin/ls -1Fh --color" # Вывод содержимого каталога в один столбец + отображение символьной инфы о типах файлов
alias lr="/bin/ls -Rh --color" # Вывод содержимого с подкаталогами
alias la="/bin/ls -lAh --color" # Подробный пывод содержимого каталога, включая скрытые файлы
alias ls="/bin/ls -hFh --color" # выделить различные типы файлов цветом
alias lx="/bin/ls -lXBh --color" # сортировка по расширению
alias lk="/bin/ls -lSrh --color" # сортировка по размеру
alias lc="/bin/ls -lcrh --color" # сортировка по времени изменения
alias lu="/bin/ls -lurh --color" # сортировка по времени последнего обращения
alias lt="/bin/ls -ltrh --color" # сортировка по дате
alias lm="/bin/ls -lAh --color | more" # вывод через "more"

# На первый взягляд это бред, но когда привыкаешь, реально ускоряет работу
alias md="/bin/mkdir"
alias mds="sudo /bin/mkdir"
alias mkdirs="sudo /bin/mkdir"
alias mdp="/bin/mkdir -p"
alias mdps="sudo /bin/mkdir -p"
alias cp="/bin/cp -iv"
alias cps="sudo /bin/cp -iv"
alias cpi="/bin/cp -v"
alias cpis="sudo /bin/cp -v"
alias cpR="/bin/cp -Rv"
alias cpRs="sudo /bin/cp -Rv"
alias cpr="/bin/cp -Riv"
alias cprs="sudo /bin/cp -Riv"
alias mv="/bin/mv -iv"
alias mvs="sudo /bin/mv -iv"
alias mvi="/bin/mv -v"
alias mvis="sudo /bin/mv -v"
alias rm="/bin/rm -iv"
alias rms="sudo /bin/rm -iv"
alias rmi="/bin/rm -v"
alias rmis="sudo /bin/rm -v"
alias rmr="/bin/rm -Rfiv"
alias rmrs="sudo /bin/rm -Rfiv"

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

alias cls="/usr/bin/clear" # Если ты привык к консоле винды, то тебе это поможет
alias h="history" # История команд
alias j="jobs -l" # Задания
alias path="echo -e ${PATH//:/\\n}" # Значение переменной окружения $PATH
alias recfg=". ~/.bashrc" # Перечитать настройки BASH (иногда бывает полезно, особенно для отладки)
alias du="/usr/bin/du -kh" # Размер всех подкаталогов в каталоге (включая др. ф.с.)
alias duS="/usr/bin/du -khxS" # Размер всех папок в каталоге (не переходить в др. ф.с.)
alias dus="/usr/bin/du -khxs" # Размер каталога (не переходить в др. ф.с.)
alias df="/bin/df -kh" # Занятое пространство на диске

alias upgrade="sudo apt-get update ; sudo apt-get upgrade" # Обновление пакетов

alias aptsearch="sudo apt-cache search " # Поиск пакетов в репозиториях
alias aptinstall="sudo apt-get install " # Установить пакет из репозитория
alias aptremove="sudo apt-get remove " # Удалить пакет из системы
alias aptpurge="sudo apt-get purge " # Удалить пакет из системы и его конф. файлы
alias aptautoremove="sudo apt-get autoremove" # Автоматически удалить не нужные пакеты из системы
alias "restart-gnome"="sudo /etc/init.d/gdm restart" # Перезапустить GNOME
alias "cd-"="cd -" # Перейти в последнюю дирректорию (бывает забываю пробем поставить)

alias ping8="ping 8.8.8.8"
alias pingya="ping ya.ru"
alias SS="sudo su -"
alias histedit="mcedit ~/.bash_history"

# Для начала определить некоторые цвета:
red="\e[0;31m" # Красный, темнее след.
RED="\e[1;31m" # Красный, светлее пред.
blue="\e[0;34m" # Синий, темнее след.
BLUE="\e[1;34m" # Синий, светлее пред.
cyan="\e[0;36m" # Циановый, темнее след.
CYAN="\e[1;36m" # Циановый, светлее пред.
NC="\e[0m" # No Color (нет цвета)

# Определил 2 раза для удобства :)
Black="\e[0;30m"
DarkGray="\e[1;30m"
Blue="\e[0;34m"
LightBlue="\e[1;34m"
Green="\e[0;32m"
LightGreen="\e[1;32m"
Cyan="\e[0;36m"
LightCyan="\e[1;36m"
Red="\e[0;31m"
LightRed="\e[1;31m"
Purple="\e[0;35m"
LightPurple="\e[1;35m"
Brown="\e[0;33m"
Yellow="\e[1;33m"
LightGray="\e[0;37m"
White="\e[1;37m"
NoColor="\e[0m"

# Лучше выглядит на черном фоне.....
echo -en "${red}"
date "+%A, %d %B %Yг. %H:%M:%S (%:::z - %Z)"
echo -en "$NC\n"

function my_ip() # IP адрес
{
    ip a | grep -e "inet " | grep -v  "host lo" > /tmp/MY_IP
    MY_ISP=$(wget -O - -q icanhazip.com)
}

function ii() # Дополнительные сведения о системе
{
    echo -e "\nИмя сервера ${RED}$HOSTNAME"
    echo -e "\nДополнительная информация:$NC " ; /bin/uname -a
    echo -e "\n${RED}В системе работают пользователи:$NC " ; /usr/bin/w -h
    echo -e "\n${RED}Дата:$NC " ; /bin/date +"%A, %d %B %Yг. %H:%M:%S (%:::z - %Z)"
    echo -e "\n${RED}Время, прошедшее с момента последней перезагрузки :$NC " ; /usr/bin/uptime
    echo -e "\n${RED}Память:$NC " ; /usr/bin/free -h
    my_ip 2>&- ;
    echo -e "\n${RED}IP адрес:$NC" ; cat /tmp/MY_IP
    echo -e "\n${RED}Адрес провайдера (ISP):$NC" ; echo ${MY_ISP:-"Соединение не установлено"}
    echo
}

function myps()
{
    ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ;
}

function pp()
{
    myps f | /usr/bin/awk "!/awk/ && $0~var" var=${1:-".*"} ;
}

