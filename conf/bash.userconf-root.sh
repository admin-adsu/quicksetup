# serg conf
    ## red prompt for root
    COLOR_WHITE="\[\033[0;00m\]"
    COLOR_RED="\[\033[1;31m\]"
    COLOR_GREEN="\[\033[0;32m\]"
    export PS1="${COLOR_RED}\u${COLOR_WHITE}@${COLOR_RED}\h:${COLOR_WHITE}\w# "
    export LC_ALL=ru_RU.UTF-8
    case "$TERM" in
        'xterm') TERM=xterm-256color;;
        'screen') TERM=screen-256color;;
        'Eterm') TERM=Eterm-256color;;
    esac
    export EDITOR=nano