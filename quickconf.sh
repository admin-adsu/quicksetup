#!/bin/bash

# TODO Сделать интерактивный режим
# TODO Сделать проверку наличя конфиг. файлов. 

# conf 
    cf_web_serv=https://gitlab.com/admin-adsu/quicksetup/-/raw/master

# init
    wget_command="/usr/bin/wget -qO /tmp/conf.txt ${cf_web_serv}"
    i=1
# ================================================================
function config_this {
    # INIT
    target_conf_file=$1
    source_conf_file=$2
    key_string=$3
    
    # if conf already contains key string
    if grep -q "$key_string" $target_conf_file 2>/dev/null; then
        echo "$target_conf_file already configured" 
        return -1
    fi 

    if [ -e  $target_conf_file ]; then 
        if $wget_command/$source_conf_file; then
            echo -e "\n>>>> Set bash user conf"
            cat /tmp/conf.txt >> $target_conf_file && rm -f /tmp/conf.txt
        else 
            echo "error download conf: $cf_web_serv/$source_conf_file"
            return -2
         fi
    else
        echo "bash user conf skipped, file $target_conf_file not exist"
        return -3
    fi 

    target_conf_file=""
    source_conf_file="" 
    key_string="" 
}

# Check if user is root
if ! [ `whoami` = "root" ]; then 
    echo "Run with root permissions"
    exit 1 
fi

# apt update
if (whiptail --title  "Step $i" --yesno  "apt update & apt upgrade?" 10 60)  then
    echo -e '\n -------------'Step $i'------------ \n'
    DEBIAN_FRONTEND=noninteractive
    apt update
    apt -y upgrade
fi; ((i=i+1)); 

# install soft
softlist="ncdu mc htop duf tree net-tools htop iftop iotop screen curl"
if (whiptail --title  "Step $i" --yesno  "install tools? ($softlist)" 10 60)  then
    apt -y install $softlist 
    wget -qP /tmp https://github.com/muesli/duf/releases/download/v0.5.0/duf_0.5.0_linux_amd64.deb && \
       dpkg -i /tmp/duf_0.5.0_linux_amd64.deb && \
       rm -f /tmp/duf_0.5.0_linux_amd64.deb
    echo -e '\n -------------'Step $i'------------ \n'
fi; ((i=i+1)); 

# locales
if (whiptail --title  "Step $i" --yesno  "Set locale to ru_RU.UTF-8?" 10 60)  then
    echo -e "\n>>>> Set Locale LANG=ru_RU.UTF-8"
    echo "LANG=ru_RU.UTF-8" > /etc/default/environment
    sed -i 's/# ru_RU.UTF-8/ru_RU.UTF-8/' /etc/locale.gen
    locale-gen 
    update-locale LANG=ru_RU.UTF-8  
    echo -e '\n -------------'Step $i'------------ \n'
fi; ((i=i+1)); 

# Timezone
if (whiptail --title  "Step $i" --yesno  "Set timezone to Europe/Moscow?" 10 60)  then
    echo -e "\n>>>> Set Timezone Europe/Moscow"
    timedatectl set-timezone Europe/Moscow
    echo -e '\n -------------'Step $i'------------ \n'
fi; ((i=i+1)); 

# bash
if (whiptail --title  "Step $i" --yesno  "Config bash?" 10 60)  then
    echo -e "\n>>>> config bash"
    config_this /root/.bashrc conf/bash.userconf-root.sh "serg conf"  
    config_this /home/serg/.bashrc conf/bash.userconf-serg.sh "serg conf"  
    config_this /etc/bash.bashrc  conf/bash.bashrc.sh "serg conf"  
    config_this /etc/inputrc  conf/bash.inputrc.sh "serg conf"  
    echo -e '\n -------------'Step $i'------------ \n'
fi; ((i=i+1)); 

# nano
if (whiptail --title  "Step $i" --yesno  "Config nano?" 10 60)  then
    echo -e "\n>>>> config nano"
    cf_nanorc=`dpkg -L nano | grep -e 'etc.*nanorc'`
    config_this $cf_nanorc  conf/nano-global.conf "serg conf"  
    touch /usr/share/nano/conf.nanorc && config_this /usr/share/nano/conf.nanorc  conf/nano-conf-hightlight.conf "serg conf"
    echo -e '\n -------------'Step $i'------------ \n'
fi; ((i=i+1)); 

# IPv6
if (whiptail --title  "Step $i" --yesno  "Disable ipv6?" 10 60)  then
    config_this /etc/sysctl.d/10-ipv6-privacy.conf  conf/ipv6.conf "serg conf"  
    sysctl -p /etc/sysctl.d/10-ipv6-privacy.conf
    echo -e '\n -------------'Step $i'------------ \n'
fi; ((i=i+1)); 

# SSH
# if (whiptail --title  "Step $i" --yesno  "Config sshd?" 10 60)  then
#     config_this /etc/ssh/sshd_config  conf/sshd.conf "serg conf"  
# fi; ((i=i+1)); 
